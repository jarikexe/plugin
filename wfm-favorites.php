<?php

/*
Plugin Name: wfm-favorites
Plugin URI: jarikexe.site
Description: Краткое описание плагина.
Version: Номер версии плагина, например: 1.0
Author: jarikexe
Author URI: jarikexe.site
*/

require __DIR__ . '/functions.php';
add_filter('the_content', 'wfm_favorites_content');
add_action('wp_enqueue_scripts', 'wfm_favorites_scripts');
